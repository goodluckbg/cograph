import java.util.*;

public final class graph<T> implements Iterable<T> {

    private final Map<T, Set<T>> firstGraph = new HashMap<T, Set<T>>();

    public graph(T[] nodeList, T[][] nodeEdges) {
        for (int i = 0; i < nodeList.length; i++) {

            addNode(nodeList[i]);
        }

        for (int i = 0; i < nodeEdges.length; i++) {
            addEdge(nodeEdges[i][0], nodeEdges[i][1]);
        }
    }

    public boolean addNode(T node) {

        if (firstGraph.containsKey(node))
            return false;

        firstGraph.put(node, new HashSet<T>());
        return true;
    }

    public Set<T> getNeighboors(T node) {

        Set<T> y = firstGraph.get(node);

        return y == null ? new TreeSet() : y;

    }

    public Set<T> getNonNeighbours(T node) {
        Set<T> neighbours = this.getNeighboors(node);
        Set<T> nonneighbours = this.firstGraph.keySet();
        nonneighbours.removeAll(neighbours);
        nonneighbours.remove(node);
        return nonneighbours;
    }

    public Set<T> allElementsInSet() {

        Set<T> allElements = this.firstGraph.keySet();
        return allElements;

    }

    public Iterator<T> iterator() {
        return firstGraph.keySet().iterator();
    }

    public boolean nodeExists(T node) {
        return firstGraph.containsKey(node);
    }

    public void addEdge(T one, T two) {

        if (!firstGraph.containsKey(one) || !firstGraph.containsKey(two))
            throw new NoSuchElementException("Both nodes must be in the graph.");

        firstGraph.get(one).add(two);
        firstGraph.get(two).add(one);

    }

    public void removeEdge(T one, T two) {
        if (!firstGraph.containsKey(one) || !firstGraph.containsKey(two))

            throw new NoSuchElementException("Both nodes must be in the graph.");

        firstGraph.get(one).remove(two);
        firstGraph.get(two).remove(one);

    }

    public boolean edgeExists(T one, T two) {

        if (!firstGraph.containsKey(one) || !firstGraph.containsKey(two))
            throw new NoSuchElementException("Both nodes must be in the graph.");

        return firstGraph.get(one).contains(two);
    }

    public int edgeExists1(T one, T two) {

        if (!firstGraph.containsKey(one) || !firstGraph.containsKey(two))
            return 0;

        return 1;
    }

    public boolean containsNode(T node) {
        return firstGraph.containsKey(node);
    }

    public boolean isEmpty() {
        return firstGraph.isEmpty();
    }

    public String toString() {
        return firstGraph.toString();
    }

    public int size() {
        return firstGraph.size();
    }
}
